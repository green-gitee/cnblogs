// test.cpp


#include <cassert>
#include <cstdlib>
#include <string>

#include "heap6.hxx"
#include "std_out.hxx"
#include "std_random.hxx"
#include "stopwatch.hxx"
#include "type_wrappers.hxx"


namespace blogs
{


static
Array<double>
time_trial(int N)
{
    Array<double> time(7);  // time[0..1] are NOT used.

    Array<Double> x(N);
    for (int i = 0; i < N; ++i) x[i] = Std_Random::random();

    Array<Double> a(N);
    for (int i = 0; i < N; ++i) a[i] = x[i];
    Stopwatch timer2;
    Heap6<2>::sort(a);
    time[2] = timer2.elapsed_time();
    assert(Heap6<2>::is_sorted(a));
    a.~Array();

    Array<Double> b(N);
    for (int i = 0; i < N; ++i) b[i] = x[i];
    Stopwatch timer3;
    Heap6<3>::sort(b);
    time[3] = timer3.elapsed_time();
    assert(Heap6<3>::is_sorted(b));
    b.~Array();

    Array<Double> c(N);
    for (int i = 0; i < N; ++i) c[i] = x[i];
    Stopwatch timer4;
    Heap6<4>::sort(c);
    time[4] = timer4.elapsed_time();
    assert(Heap6<4>::is_sorted(c));
    c.~Array();

    Array<Double> d(N);
    for (int i = 0; i < N; ++i) d[i] = x[i];
    Stopwatch timer5;
    Heap6<5>::sort(d);
    time[5] = timer5.elapsed_time();
    assert(Heap6<5>::is_sorted(d));
    d.~Array();

    Array<Double> e(N);
    for (int i = 0; i < N; ++i) e[i] = x[i];
    Stopwatch timer6;
    Heap6<6>::sort(e);
    time[6] = timer6.elapsed_time();
    assert(Heap6<6>::is_sorted(e));
    e.~Array();

    return time;
}


static
void
test(char * argv[])
{
    int T = std::stoi(argv[1]);
    Array<double> prev = time_trial(512);
    Std_Out::printf("%10s%14s%7s%14s%7s%14s%7s%14s%7s%14s%7s\n", "N", "2-way Time", "Ratio", "3-way Time", "Ratio", "4-way Time", "Ratio", "5-way Time", "Ratio", "6-way Time", "Ratio");
    for (int i = 0, N = 1024; i < T; ++i, N += N) {
        Array<double> time = time_trial(N);
        Std_Out::printf("%10d%14.3f%7.2f%14.3f%7.2f%14.3f%7.2f%14.3f%7.2f%14.3f%7.2f\n", N, time[2], time[2]/prev[2], time[3], time[3]/prev[3], time[4], time[4]/prev[4], time[5], time[5]/prev[5], time[6], time[6]/prev[6]);
        prev = time;
    }
}


} // end of namespace blogs


int
main(int argc, char * argv[])
{
    blogs::Std_Out::println();

    if (argc != 2) {
        blogs::Std_Out::println(
            "Usage: /path/to/a.out <T>"
        "\n");
        std::exit(EXIT_FAILURE);
    }

    blogs::test(argv);

    blogs::Std_Out::println();
    return 0;
}

