// stopwatch.cpp


#include "stopwatch.hxx"

#include <chrono>


namespace blogs
{


using std::chrono::milliseconds;


Stopwatch::Stopwatch()
    : __start__(steady_clock::now())
{
}


double
Stopwatch::elapsed_time() const
{
    time_point<steady_clock> now = steady_clock::now();
    return std::chrono::duration_cast<milliseconds>(now - __start__).count() / 1000.0;
}


} // end of namespace blogs

