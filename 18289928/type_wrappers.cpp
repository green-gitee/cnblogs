// type_wrappers.cpp


#include "type_wrappers.hxx"


namespace blogs
{


Boolean::Boolean(bool v)
    : __v__(v)
{
}


bool
Boolean::value() const
{
    return __v__;
}


int
Boolean::compare_to(Boolean const& that) const
{
    if (__v__ > that.__v__) return +1;
    if (__v__ < that.__v__) return -1;
    return 0;
}


string
Boolean::to_string() const
{
    string s;
    // s += typeid(Boolean).name();
    // s += ": ";
    s += (__v__) ? "true" : "false";
    return s;
}


bool
operator==(Boolean const& lhs, Boolean const& rhs)
{
    return lhs.__v__ == rhs.__v__;
}


bool
operator!=(Boolean const& lhs, Boolean const& rhs)
{
    return lhs.__v__ != rhs.__v__;
}


ostream &
operator<<(ostream & os, Boolean const& b)
{
    os << b.to_string();
    return os;
}


Integer::Integer(int v)
    : __v__(v)
{
}


int
Integer::value() const
{
    return __v__;
}


int
Integer::compare_to(Integer const& that) const
{
    if (__v__ > that.__v__) return +1;
    if (__v__ < that.__v__) return -1;
    return 0;
}


string
Integer::to_string() const
{
    string s;
    // s += typeid(Integer).name();
    // s += ": ";
    s += std::to_string(__v__);
    return s;
}


bool
operator==(Integer const& lhs, Integer const& rhs)
{
    return lhs.__v__ == rhs.__v__;
}


bool
operator!=(Integer const& lhs, Integer const& rhs)
{
    return lhs.__v__ != rhs.__v__;
}


ostream &
operator<<(ostream & os, Integer const& i)
{
    os << i.to_string();
    return os;
}


Long::Long(long v)
    : __v__(v)
{
}


long
Long::value() const
{
    return __v__;
}


int
Long::compare_to(Long const& that) const
{
    if (__v__ > that.__v__) return +1;
    if (__v__ < that.__v__) return -1;
    return 0;
}


string
Long::to_string() const
{
    string s;
    // s += typeid(Long).name();
    // s += ": ";
    s += std::to_string(__v__);
    return s;
}


bool
operator==(Long const& lhs, Long const& rhs)
{
    return lhs.__v__ == rhs.__v__;
}


bool
operator!=(Long const& lhs, Long const& rhs)
{
    return lhs.__v__ != rhs.__v__;
}


ostream &
operator<<(ostream & os, Long const& l)
{
    os << l.to_string();
    return os;
}


Double::Double(double v)
    : __v__(v)
{
}


double
Double::value() const
{
    return __v__;
}


int
Double::compare_to(Double const& that) const
{
    if (__v__ > that.__v__) return +1;
    if (__v__ < that.__v__) return -1;
    return 0;
}


string
Double::to_string() const
{
    string s;
    // s += typeid(Double).name();
    // s += ": ";
    s += std::to_string(__v__);
    return s;
}


bool
operator==(Double const& lhs, Double const& rhs)
{
    return lhs.__v__ == rhs.__v__;
}


bool
operator!=(Double const& lhs, Double const& rhs)
{
    return lhs.__v__ != rhs.__v__;
}


ostream &
operator<<(ostream & os, Double const& d)
{
    os << d.to_string();
    return os;
}


Character::Character(char v)
    : __v__(v)
{
}


char
Character::value() const
{
    return __v__;
}


int
Character::compare_to(Character const& that) const
{
    if (__v__ > that.__v__) return +1;
    if (__v__ < that.__v__) return -1;
    return 0;
}


string
Character::to_string() const
{
    string s;
    // s += typeid(String).name();
    // s += ": ";
    s += __v__;
    return s;
}


bool
operator==(Character const& lhs, Character const& rhs)
{
    return lhs.__v__ == rhs.__v__;
}


bool
operator!=(Character const& lhs, Character const& rhs)
{
    return lhs.__v__ != rhs.__v__;
}


ostream &
operator<<(ostream & os, Character const& s)
{
    os << s.to_string();
    return os;
}



String::String(string v)
    : __v__(v)
{
}


string
String::value() const
{
    return __v__;
}


int
String::compare_to(String const& that) const
{
    if (__v__ > that.__v__) return +1;
    if (__v__ < that.__v__) return -1;
    return 0;
}


string
String::to_string() const
{
    string s;
    // s += typeid(String).name();
    // s += ": ";
    s += __v__;
    return s;
}


bool
operator==(String const& lhs, String const& rhs)
{
    return lhs.__v__ == rhs.__v__;
}


bool
operator!=(String const& lhs, String const& rhs)
{
    return lhs.__v__ != rhs.__v__;
}


ostream &
operator<<(ostream & os, String const& s)
{
    os << s.to_string();
    return os;
}


} // end of namespace blogs


