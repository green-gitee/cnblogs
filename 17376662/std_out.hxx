/*
 * std_out.hxx
 *
 * Demonstrating the compiler warning:
 * explicit specialization cannot have a storage class.
 *
 */

#ifndef _STD_OUT
#define _STD_OUT


#include <iostream>


using std::cout;
using std::endl;


class Std_Out
{

  public:

    /*
     * Print a newline.
     *
     */
    static void println()
    {
        cout << endl;
    }


    /*
     * Print a value.
     *
     */
    template <class _T>
    static void print(_T v)
    {
        cout.width(8);
        cout << v;
    }


    /*
     * Print a boolean value.
     *
     * NOTE:
     *  compile warning : explicit specialization cannot have a storage class
     *
     *  Solution 1: 'static' is commented.
     *
     */
    template <>
    //
    // NOTE:
    //  compile warning : explicit specialization cannot have a storage class
    //
    //  Solution 1: 'static' is commented.
    //
    //static
    void print<bool>(bool v)
    {
        cout.width(8);
        cout << (v ? "true" : "false");
    }


    /*
     * Print a value ending with a newline.
     *
     */
    template <class _T>
    static void println(_T v)
    {
        cout.width(8);
        cout << v << endl;
    }


    /*
     * Print a boolean value ending with a newline.
     *
     * NOTE:
     *  compile warning : explicit specialization cannot have a storage class
     *
     *  Solution 2: definition is moved to cpp file.
     *
     */
    //template <>
    //static void println<bool>(bool v)
    //{
    //    cout.width(8);
    //    cout << (v ? "true" : "false") << endl;
    //}


};


#endif

