/*
 * main.cpp
 *
 * Demonstrating the compiler warning:
 * explicit specialization cannot have a storage class.
 *
 */

#include "std_out.hxx"

/*
 * Print a boolean value ending with a newline.
 *
 */
template <>
void Std_Out::println<bool>(bool v)
{
    cout.width(8);
    cout << (v ? "true" : "false") << endl;
}


const int N = 3;
const int M = 4;


int main()
{
    Std_Out::println();

    char mc[N][M] = {
        { 'A', 'B', 'C', 'D' },
        { 'E', 'F', 'G', 'H' },
        { 'I', 'J', 'K', 'L' },
    };
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < M; ++j) {
            Std_Out::print<char>(mc[i][j]);     // Using template explicit specialization to print a boolean value.
        }
        Std_Out::println();
    }

    Std_Out::println();
    Std_Out::println();

    int mi[N][M] = {
        {  1,  2,  3,  4 },
        { 11, 12, 13, 14 },
        { 21, 22, 23, 24 },
    };

    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < M; ++j) {
            Std_Out::print<int>(mi[i][j]);     // Using template explicit specialization to print a boolean value.
        }
        Std_Out::println();
    }

    Std_Out::println();
    Std_Out::println();

    bool mb[N][M] = {
        { false,  true, false,  true },
        {  true, false,  true, false },
        { false,  true, false,  true },
    };

    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < M; ++j) {
            Std_Out::print<bool>(mb[i][j]);     // Using template explicit specialization to print a boolean value.
        }
        Std_Out::println();
    }

    Std_Out::println();
    return 0;
}

