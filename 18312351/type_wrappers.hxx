// type_wrappers.hxx


#ifndef _BLOGS_WRAPPED_TYPES
#define _BLOGS_WRAPPED_TYPES


#include <iostream>
#include <string>

#include "comparable.hxx"


namespace blogs
{


using std::string;
using std::ostream;


class Boolean
    : public Comparable<Boolean>
{

  private:

    bool
    __v__;


  public:

    Boolean(bool = false);


    bool
    value() const;


    int
    compare_to(Boolean const&) const;


    string
    to_string() const;


    friend
    bool
    operator==(Boolean const&, Boolean const&);


    friend
    bool
    operator!=(Boolean const&, Boolean const&);


    friend
    ostream &
    operator<<(ostream &, Boolean const&);

};


class Integer
    : public Comparable<Integer>
{

  private:

    int
    __v__;


  public:

    Integer(int = 0);


    int
    value() const;


    int
    compare_to(Integer const&) const;


    string
    to_string() const;


    friend
    bool
    operator==(Integer const&, Integer const&);


    friend
    bool
    operator!=(Integer const&, Integer const&);


    friend
    ostream &
    operator<<(ostream &, Integer const&);

};


class Long
    : public Comparable<Long>
{

  private:

    long
    __v__;


  public:

    Long(long = 0L);


    long
    value() const;


    int
    compare_to(Long const&) const;


    string
    to_string() const;


    friend
    bool
    operator==(Long const&, Long const&);


    friend
    bool
    operator!=(Long const&, Long const&);


    friend
    ostream &
    operator<<(ostream &, Long const&);

};


class Double
    : public Comparable<Double>
{

  private:

    double
    __v__;


  public:

    Double(double = 0.0);


    double
    value() const;


    int
    compare_to(Double const&) const;


    string
    to_string() const;


    friend
    bool
    operator==(Double const&, Double const&);


    friend
    bool
    operator!=(Double const&, Double const&);


    friend
    ostream &
    operator<<(ostream &, Double const&);

};


class Character
    : public Comparable<Character>
{

  private:

    char
    __v__;


  public:

    Character(char = ' ');


    char
    value() const;


    int
    compare_to(Character const&) const;


    string
    to_string() const;


    friend
    bool
    operator==(Character const&, Character const&);


    friend
    bool
    operator!=(Character const&, Character const&);


    friend
    ostream &
    operator<<(ostream &, Character const&);

};


class String
    : public Comparable<String>
{

  private:

    string
    __v__;


  public:

    String(string = "");


    string
    value() const;


    int
    compare_to(String const&) const;


    string
    to_string() const;


    friend
    bool
    operator==(String const&, String const&);


    friend
    bool
    operator!=(String const&, String const&);


    friend
    ostream &
    operator<<(ostream &, String const&);

};


} // end of namespace blogs


#endif

