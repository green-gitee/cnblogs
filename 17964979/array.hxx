// array.hxx


#ifndef _BLOG_ARRAY
#define _BLOG_ARRAY


#include <initializer_list>
#include <memory>


namespace blog
{


using std::default_delete;
using std::initializer_list;
using std::shared_ptr;


template <class _T>
class Array
{

  private:

    size_t
    __size__;


    shared_ptr<_T>
    __buffer__;


  public:

    Array(size_t size = 0)
        : __size__(size), __buffer__(nullptr)
    {
        if (__size__ < 0)
            throw;
        __buffer__ = shared_ptr<_T>(new _T[__size__] {},
                                    default_delete<_T[]>());
    }


    Array(initializer_list<_T> init)
        : __size__(init.size()), __buffer__(nullptr)
    {
        __buffer__ = shared_ptr<_T>(new _T[__size__] {},
                                    default_delete<_T[]>());
        size_t i = 0;
        for (_T x : init)
            __buffer__.get()[i++] = x;
    }


    Array(Array const& other)
        : __size__(other.__size__), __buffer__(other.__buffer__)
    {
    }


    Array(Array && other)
        : __size__(other.__size__), __buffer__(other.__buffer__)
    {
        other.__size__ = 0;
        other.__buffer__ = nullptr;
    }


    ~Array()
    {
    }


    Array &
    operator=(Array const& other)
    {
        if (this == &other)
            return *this;
        __size__ = other.__size__;
        __buffer__ = other.__buffer__;
        return *this;
    }


    Array &
    operator=(Array && other)
    {
        if (this == &other)
            return *this;
        __size__ = other.__size__;
        __buffer__ = other.__buffer__;
        other.__size__ = 0;
        other.__buffer__ = nullptr;
        return *this;
    }


    size_t
    size() const
    {
        return __size__;
    }


    _T const&
    operator[](size_t idx) const
    {
        return __buffer__.get()[idx];
    }


    _T &
    operator[](size_t idx)
    {
        return const_cast<_T &>(static_cast<Array const&>(*this)[idx]);
    }


    _T const*
    begin() const
    {
        return __buffer__.get();
    }


    _T *
    begin()
    {
        return const_cast<_T *>(static_cast<Array const&>(*this).begin());
    }


    _T const*
    end() const
    {
        return __buffer__.get() + __size__;
    }


    _T *
    end()
    {
        return const_cast<_T *>(static_cast<Array const&>(*this).end());
    }

};


} // end of namespace blog


#endif

