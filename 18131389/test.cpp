// test.cpp


#include <cassert>
#include <cstdlib>
#include <string>

#include "bubble2.hxx"
#include "std_out.hxx"
#include "std_random.hxx"
#include "stopwatch.hxx"
#include "type_wrappers.hxx"


namespace blogs
{


static
double
time_trial(int N)
{
    Array<Double> a(N);
    for (int i = 0; i < N; ++i) a[i] = Std_Random::random();
    Stopwatch timer;
    Bubble2::sort(a);
    double time = timer.elapsed_time();
    assert(Bubble2::is_sorted(a));
    return time;
}


static
void
test(char * argv[])
{
    int T = std::stoi(argv[1]);
    double prev = time_trial(512);
    Std_Out::printf("%10s%10s%7s\n", "N", "Time", "Ratio");
    for (int i = 0, N = 1024; i < T; ++i, N += N) {
        double time = time_trial(N);
        Std_Out::printf("%10d%10.3f%7.1f\n", N, time, time/prev);
        prev = time;
    }
}


} // end of namespace blogs


int
main(int argc, char * argv[])
{
    blogs::Std_Out::println();

    if (argc != 2) {
        blogs::Std_Out::println(
            "Usage: /path/to/a.out <T>"
        "\n");
        std::exit(EXIT_FAILURE);
    }

    blogs::test(argv);

    blogs::Std_Out::println();
    return 0;
}

