// merge4.hxx


#ifndef _BLOGS_MERGE4
#define _BLOGS_MERGE4


#include <type_traits>

#include "array.hxx"
#include "comparable.hxx"
#include "std_out.hxx"


namespace blogs
{


class Merge4
{

  private:

    template
    <
        class _T,
        class = typename std::enable_if<std::is_base_of<Comparable<_T>, _T>::value>::type
    >
    static
    bool
    __less__(_T const& v, _T const& w)
    {
        return v.compare_to(w) < 0;
    }


    template
    <
        class _T,
        class = typename std::enable_if<std::is_base_of<Comparable<_T>, _T>::value>::type
    >
    static
    void
    __exch__(Array<_T> & a, int i, int j)
    {
        _T t = a[i];
        a[i] = a[j];
        a[j] = t;
    }


    template
    <
        class _T,
        class = typename std::enable_if<std::is_base_of<Comparable<_T>, _T>::value>::type
    >
    static
    void
    __merge__(Array<_T> & a, int lo, int mi, int hi, Array<_T> & aux)
    {
        int i = lo, j = hi;
        for (int k = lo; k <= mi; ++k)
            aux[k] = a[k];
        for (int k = mi+1; k <= hi; ++k)
            aux[k] = a[hi - (k - (mi+1))];
        for (int k = lo; i <= j; ++k)
            if (__less__(aux[i], aux[j])) a[k] = aux[i++];
            else                          a[k] = aux[j--];
    }


    template
    <
        class _T,
        class = typename std::enable_if<std::is_base_of<Comparable<_T>, _T>::value>::type
    >
    static
    void
    __sort__(Array<_T> & a, int lo, int hi, Array<_T> & aux)
    {
        if (hi - lo <= 15) {
            for (int i = lo+1; i <= hi; ++i)
                for (int j = i; j > lo && __less__(a[j], a[j-1]); --j)
                    __exch__(a, j, j-1);
        } else {
            int mi = lo + (hi - lo) / 2;
            __sort__(a, lo, mi, aux);
            __sort__(a, mi+1, hi, aux);
            if (__less__(a[mi], a[mi+1]))
                for (int i = lo; i <= hi; ++i)
                    aux[i] = a[i];
            else
                __merge__(a, lo, mi, hi, aux);
        }
    }


  public:

    template
    <
        class _T,
        class = typename std::enable_if<std::is_base_of<Comparable<_T>, _T>::value>::type
    >
    static
    void
    sort(Array<_T> & a)
    {
        Array<_T> aux = Array<_T>(a.size());
        __sort__(a, 0, a.size()-1, aux);
    }


    template
    <
        class _T,
        class = typename std::enable_if<std::is_base_of<Comparable<_T>, _T>::value>::type
    >
    static
    bool
    is_sorted(Array<_T> const& a)
    {
        int N = a.size();
        for (int i = 1; i < N; ++i)
            if (__less__(a[i], a[i-1])) return false;
        return true;
    }

};


} // end of namespace blogs


#endif

