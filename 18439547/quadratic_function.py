#!/usr/bin/env python3
#
# quadratic_function.py
#


from matplotlib.ticker import FuncFormatter
from matplotlib.ticker import MultipleLocator
import matplotlib.pyplot as plt
import numpy as np


def main():

    fig, axs = plt.subplots(1, 3, figsize=(14,4.5))

    axs[0] = configure_axes(axs[0], 'Quadratic Function\t\t\t' + r'$\Delta > 0$', 18, 18, 10, 2, 10, 2)
    x = np.linspace(-2.5,14.5,100)
    y = 1/3*x**2 - 4*x
    axs[0].plot(x, y, color='b')
    axs[0].text(-4, 4,  r'$y = \frac{1}{3}x^2 - 4x$',  color='b', horizontalalignment='right',  verticalalignment='bottom')

    axs[1] = configure_axes(axs[1], '\t\t\t\t\t\t\t' + r'$\Delta = 0$', 18, 18, 10, 2, 10, 2)
    x = np.linspace(-5,11,100)
    y = -1/4*x**2 + 3/2*x - 9/4
    axs[1].plot(x, y, color='r')
    axs[1].text(-4, -6,  r'$y = -\frac{1}{4}x^2 + \frac{3}{2}x - \frac{9}{4}$',  color='r', horizontalalignment='right',  verticalalignment='bottom')

    axs[2] = configure_axes(axs[2], '\t\t\t\t\t\t\t' + r'$\Delta < 0$', 18, 18, 10, 2, 10, 2)
    x = np.linspace(-9.8,-2.2,100)
    y = x**2 + 12*x + 39
    axs[2].plot(x, y, color='y')
    axs[2].text(-7, 1,  r'$y = x^2 + 12x + 39$',  color='y', horizontalalignment='right',  verticalalignment='bottom')

    fig.tight_layout()

    plt.show()
    # end of main()


def configure_axes(ax, title, xlimit, ylimit, xmajorunit = 5, xminorunit = 1, ymajorunit = 5, yminorunit = 1):

    ax.set_title(title, fontsize='large', loc='left')

    ax.spines['left'].set_position('zero')
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_position('zero')
    ax.spines['top'].set_visible(False)

    ax.text(-0.2, -0.2, 'O', fontsize='x-large', fontstyle='italic', horizontalalignment='right', verticalalignment='top')

    xlimit += xmajorunit*0.8
    ylimit += ymajorunit*0.8
    ax.set_xlim((-xlimit, xlimit))
    ax.set_ylim((-ylimit, ylimit))

    ax.plot(1, 0, '>k', transform=ax.get_yaxis_transform(), clip_on=False)
    ax.xaxis.set_label_text('x')
    ax.xaxis.set_label_coords(1.01, 0.48)
    ax.xaxis.set_major_formatter(FuncFormatter(lambda w, pos: int(w) if w else ''))
    ax.xaxis.set_major_locator(MultipleLocator(xmajorunit))
    ax.xaxis.set_minor_locator(MultipleLocator(xminorunit))
    ax.xaxis.set_tick_params(which='both', direction='in')

    ax.plot(0, 1, '^k', transform=ax.get_xaxis_transform(), clip_on=False)
    ax.yaxis.set_label_text('y')
    ax.yaxis.set_label_coords(0.48, 1)
    ax.yaxis.label.set_rotation(0)
    ax.yaxis.set_major_formatter(FuncFormatter(lambda w, pos: int(w) if w else ''))
    ax.yaxis.set_major_locator(MultipleLocator(ymajorunit))
    ax.yaxis.set_minor_locator(MultipleLocator(yminorunit))
    ax.yaxis.set_tick_params(which='both', direction='in')

    return ax
    # end of configure_axes()


if __name__ == '__main__': main()

