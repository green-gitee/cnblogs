// test.cpp


#include <cassert>
#include <cmath>
#include <cstdlib>
#include <string>

#include "merge6.hxx"
#include "std_out.hxx"
#include "std_random.hxx"
#include "stopwatch.hxx"
#include "type_wrappers.hxx"


namespace blogs
{


static
double
time_trial(int K, int N)
{
    Array<Double> a(N);
    for (int i = 0; i < N; ++i) a[i] = Std_Random::random();
    Stopwatch timer;
    Merge6::sort(K, a);
    double time = timer.elapsed_time();
    assert(Merge6::is_sorted(a));
    return time;
}


static
void
test(char * argv[])
{
    int T = std::stoi(argv[1]);
    Std_Out::printf("%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s%10s\n", "N", "K = 2", "K = 3", "K = 4", "K = 5", "K = 6", "K = 7", "K = 8", "K = 9", "K = 10", "K = 11");
    for (int i = 0, N = 1024; i < T; ++i, N += N) {
        double time2  = time_trial(2,  N);
        double time3  = time_trial(3,  N);
        double time4  = time_trial(4,  N);
        double time5  = time_trial(5,  N);
        double time6  = time_trial(6,  N);
        double time7  = time_trial(7,  N);
        double time8  = time_trial(8,  N);
        double time9  = time_trial(9,  N);
        double time10 = time_trial(10, N);
        double time11 = time_trial(11, N);
        Std_Out::printf("%10d%10.3f%10.3f%10.3f%10.3f%10.3f%10.3f%10.3f%10.3f%10.3f%10.3f\n", N, time2, time3, time4, time5, time6, time7, time8, time9, time10, time11);
    }
}


} // end of namespace blogs


int
main(int argc, char * argv[])
{
    blogs::Std_Out::println();

    if (argc != 2) {
        blogs::Std_Out::println(
            "Usage: /path/to/a.out <T>"
        "\n");
        std::exit(EXIT_FAILURE);
    }

    blogs::test(argv);

    blogs::Std_Out::println();
    return 0;
}

