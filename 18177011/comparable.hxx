// comparable.hxx


#ifndef _BLOGS_COMPARABLE
#define _BLOGS_COMPARABLE


namespace blogs
{


template <class _T>
class Comparable
{

  public:

    virtual
    int
    compare_to(_T const&) const
    = 0;

};


} // end of namespace blogs


#endif

