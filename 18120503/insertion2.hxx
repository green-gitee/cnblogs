// insertion2.hxx


#ifndef _BLOGS_INSERTION2
#define _BLOGS_INSERTION2


#include <type_traits>

#include "array.hxx"
#include "comparable.hxx"
#include "std_out.hxx"


namespace blogs
{


class Insertion2
{

  private:

    template
    <
        class _T,
        class = typename std::enable_if<std::is_base_of<Comparable<_T>, _T>::value>::type
    >
    static
    bool
    __less__(_T const& v, _T const& w)
    {
        return v.compare_to(w) < 0;
    }


  public:

    template
    <
        class _T,
        class = typename std::enable_if<std::is_base_of<Comparable<_T>, _T>::value>::type
    >
    static
    void
    sort(Array<_T> & a)
    {
        int N = a.size();
        for (int i = 1; i < N; ++i) {
            _T t = a[i];
            int j;
            for (j = i; j > 0 && __less__(t, a[j-1]); --j)
                a[j] = a[j-1];
            a[j] = t;
        }
    }


    template
    <
        class _T,
        class = typename std::enable_if<std::is_base_of<Comparable<_T>, _T>::value>::type
    >
    static
    bool
    is_sorted(Array<_T> const& a)
    {
        int N = a.size();
        for (int i = 1; i < N; ++i)
            if (__less__(a[i], a[i-1])) return false;
        return true;
    }

};


} // end of namespace blogs


#endif

