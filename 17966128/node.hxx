// node.hxx


#ifndef _BLOG_NODE
#define _BLOG_NODE


namespace blog
{


template <class _T>
struct Node
{

    _T
    item;


    Node *
    next = nullptr;

};


template <class _T>
struct Biway_Node
{

    _T
    item;


    Biway_Node *
    prev = nullptr;


    Biway_Node *
    next = nullptr;

};


} // end of namespace blog


#endif

