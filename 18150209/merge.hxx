// merge.hxx


#ifndef _BLOGS_MERGE
#define _BLOGS_MERGE


#include <type_traits>

#include "array.hxx"
#include "comparable.hxx"
#include "std_out.hxx"


namespace blogs
{


class Merge
{

  private:

    template
    <
        class _T,
        class = typename std::enable_if<std::is_base_of<Comparable<_T>, _T>::value>::type
    >
    static
    bool
    __less__(_T const& v, _T const& w)
    {
        return v.compare_to(w) < 0;
    }


    template
    <
        class _T,
        class = typename std::enable_if<std::is_base_of<Comparable<_T>, _T>::value>::type
    >
    static
    void
    __merge__(Array<_T> & a, int lo, int mi, int hi, Array<_T> & aux)
    {
        int i = lo, j = mi+1;
        for (int k = lo; k <= hi; ++k)
            aux[k] = a[k];
        for (int k = lo; k <= hi; ++k)
            if      (i > mi)                   a[k] = aux[j++];
            else if (j > hi)                   a[k] = aux[i++];
            else if (__less__(aux[j], aux[i])) a[k] = aux[j++];
            else                               a[k] = aux[i++];
    }


    template
    <
        class _T,
        class = typename std::enable_if<std::is_base_of<Comparable<_T>, _T>::value>::type
    >
    static
    void
    __sort__(Array<_T> & a, int lo, int hi, Array<_T> & aux)
    {
        if (hi <= lo) return;
        int mi = lo + (hi - lo) / 2;
        __sort__(a, lo, mi, aux);
        __sort__(a, mi+1, hi, aux);
        __merge__(a, lo, mi, hi, aux);
    }


  public:

    template
    <
        class _T,
        class = typename std::enable_if<std::is_base_of<Comparable<_T>, _T>::value>::type
    >
    static
    void
    sort(Array<_T> & a)
    {
        Array<_T> aux = Array<_T>(a.size());
        __sort__(a, 0, a.size()-1, aux);
    }


    template
    <
        class _T,
        class = typename std::enable_if<std::is_base_of<Comparable<_T>, _T>::value>::type
    >
    static
    bool
    is_sorted(Array<_T> const& a)
    {
        int N = a.size();
        for (int i = 1; i < N; ++i)
            if (__less__(a[i], a[i-1])) return false;
        return true;
    }

};


} // end of namespace blogs


#endif

