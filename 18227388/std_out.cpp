// std_out.cpp


#include "std_out.hxx"

#include <cstdarg>
#include <cstdio>


namespace blogs
{


void
Std_Out::println()
{
    cout << endl;
}


void
Std_Out::printf(char const* fmt, ...)
{
    std::va_list args;
    va_start(args, fmt);

    std::vfprintf(stdout, fmt, args);
    // or
    // std::vprintf(fmt, args);

    va_end(args);
}


} // end of namespace blogs

