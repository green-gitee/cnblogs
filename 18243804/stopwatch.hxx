// stopwatch.hxx


#ifndef _BLOGS_STOPWATCH
#define _BLOGS_STOPWATCH


#include <chrono>


namespace blogs
{


using std::chrono::steady_clock;
using std::chrono::time_point;


class Stopwatch
{

  private:

    time_point<steady_clock> const
    __start__;


  public:

    Stopwatch();


    double
    elapsed_time() const;

};


} // end of namespace blogs


#endif

