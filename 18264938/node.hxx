// node.hxx


#ifndef _BLOGS_NODE
#define _BLOGS_NODE


namespace blogs
{


template <class _T>
struct Node
{

    _T
    item;


    Node *
    next = nullptr;

};


} // end of namespace blogs


#endif

