#!/usr/bin/env python3
#
# logarithm_axes.py
#


from matplotlib.ticker import FuncFormatter
import math
import matplotlib.pyplot as plt
import numpy as np


def main():

    N = np.array([  1024,  2048,  4096,   8192,  16384,   32768,    65536,   131072 ])
    S = np.array([ 0.134, 0.537, 2.151,  8.599, 34.415, 137.924,  533.501, 2222.169 ])
    B = np.array([ 0.300, 1.198, 4.777, 19.069, 76.458, 305.503, 1213.778, 4877.336 ])
    Q = np.array([ 0.006, 0.014, 0.029,  0.061,  0.127,   0.271,    0.564,    1.196 ])

    xfmtr = FuncFormatter(lambda x, pos: '{:1.0f}K'.format(x*1e-3) if x >= 1e3 else x)
    yfmtr = FuncFormatter(lambda y, pos: '{:1.0f}K'.format(y*1e-3) if y >= 1e3 else y)

    fig, axs = plt.subplots(4, 3, figsize=(12,9))

    axs[0,0].plot(N, S)
    axs[0,0].set(title='Selection\n Normal Scalar')
    axs[0,0].set(xlabel='N', ylabel='Time')
    axs[0,0].grid()
    axs[0,0].xaxis.set_major_formatter(xfmtr)
    axs[0,0].yaxis.set_major_formatter(yfmtr)

    axs[0,1].plot(N, B)
    axs[0,1].set(title='Bubble2\n Normal Scalar')
    axs[0,1].grid()
    axs[0,1].xaxis.set_major_formatter(xfmtr)
    axs[0,1].yaxis.set_major_formatter(yfmtr)

    axs[0,2].plot(N, Q)
    axs[0,2].set(title='Quick4\n Normal Scalar')
    axs[0,2].grid()
    axs[0,2].xaxis.set_major_formatter(xfmtr)
    axs[0,2].yaxis.set_major_formatter(yfmtr)

    axs[1,0].loglog(N, S, basex=2, basey=2)
    axs[1,0].set(title='Log Scalar on X & Y')
    axs[1,0].grid()
    axs[1,0].xaxis.set_major_formatter(xfmtr)
    axs[1,0].yaxis.set_major_formatter(yfmtr)

    axs[1,1].loglog(N, B, basex=2, basey=2)
    axs[1,1].set(title='Log Scalar on X & Y')
    axs[1,1].grid()
    axs[1,1].xaxis.set_major_formatter(xfmtr)
    axs[1,1].yaxis.set_major_formatter(yfmtr)

    axs[1,2].loglog(N, Q, basex=2, basey=2)
    axs[1,2].set(title='Log Scalar on X & Y')
    axs[1,2].grid()
    axs[1,2].xaxis.set_major_formatter(xfmtr)
    axs[1,2].yaxis.set_major_formatter(yfmtr)

    axs[2,0].semilogx(N, S, basex=2)
    axs[2,0].set(title='Log Scalar on X')
    axs[2,0].grid()
    axs[2,0].xaxis.set_major_formatter(xfmtr)
    axs[2,0].yaxis.set_major_formatter(yfmtr)

    axs[2,1].semilogx(N, B, basex=2)
    axs[2,1].set(title='Log Scalar on X')
    axs[2,1].grid()
    axs[2,1].xaxis.set_major_formatter(xfmtr)
    axs[2,1].yaxis.set_major_formatter(yfmtr)

    axs[2,2].semilogx(N, Q, basex=2)
    axs[2,2].set(title='Log Scalar on X')
    axs[2,2].grid()
    axs[2,2].xaxis.set_major_formatter(xfmtr)
    axs[2,2].yaxis.set_major_formatter(yfmtr)

    axs[3,0].semilogy(N, S, basey=2)
    axs[3,0].set(title='Log Scalar on Y')
    axs[3,0].grid()
    axs[3,0].xaxis.set_major_formatter(xfmtr)
    axs[3,0].yaxis.set_major_formatter(yfmtr)

    axs[3,1].semilogy(N, B, basey=2)
    axs[3,1].set(title='Log Scalar on Y')
    axs[3,1].grid()
    axs[3,1].xaxis.set_major_formatter(xfmtr)
    axs[3,1].yaxis.set_major_formatter(yfmtr)

    axs[3,2].semilogy(N, Q, basey=2)
    axs[3,2].set(title='Log Scalar on Y')
    axs[3,2].grid()
    axs[3,2].xaxis.set_major_formatter(xfmtr)
    axs[3,2].yaxis.set_major_formatter(yfmtr)

    fig.tight_layout()
    plt.show()


if __name__ == '__main__':
    main()
